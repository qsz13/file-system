package com.qsz.GUI.Icon;

import com.qsz.Model.File;
import com.qsz.Exception.NodeNotExistException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by danielqiu on 6/3/14.
 */
public class FileIcon extends NodeIcon {

    private File file;
    private FileIcon thisFileIcon = this;


    public FileIcon(File file) {
        super(file);

        this.image = new ImageIcon(File.class.getResource("/file_icon.png"));
        this.iconLabel.setIcon(this.image);

        this.file = file;


    }


    protected void addListener() {
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {

                if (e.isPopupTrigger()) {
                    showPopupMenu(e);
                }

            }

            public void mouseReleased(MouseEvent e) {

                if (e.isPopupTrigger()) {
                    showPopupMenu(e);
                }

            }

            public void mouseEntered(MouseEvent e) {
                thisFileIcon.setBackground(Color.lightGray);

            }

            public void mouseExited(MouseEvent e) {
                thisFileIcon.setBackground(null);
            }


        });

        this.deleteItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    fileExplorer.getDisk().deleteFile(file);
                    file.removeSelfFromParent();
                    fileExplorer.getDirectoryView().refreshView();
                } catch (NodeNotExistException e1) {
                    JOptionPane.showMessageDialog(fileExplorer,
                            e1.getMessage(),
                            "file does not exist",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        this.renameItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String newFileName = JOptionPane.showInputDialog("enter new directory name",file.getName());
                if(newFileName != null)
                {
                    file.setName(newFileName);
                }

                fileExplorer.getDirectoryView().refreshView();
            }
        });
    }




}
