package com.qsz.GUI.Icon;

import com.qsz.GUI.FileExplorer;
import com.qsz.Model.Node;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created by danielqiu on 6/4/14.
 */
public abstract class NodeIcon extends JPanel {

    protected ImageIcon image;

    protected JPopupMenu popupMenu = new JPopupMenu();
    protected JMenuItem deleteItem = new JMenuItem("delete");
    protected JMenuItem renameItem = new JMenuItem("rename");
    protected JLabel iconLabel = new JLabel();
    protected JLabel nameLabel = new JLabel();

    protected FileExplorer fileExplorer;

    public NodeIcon(Node node)
    {
        this.nameLabel.setText(node.getName());
        this.nameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.iconLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(iconLabel);
        this.add(nameLabel);

        this.popupMenu.add(deleteItem);
        this.popupMenu.add(renameItem);

        this.addListener();

    }


    protected void addListener(){}
    protected void showPopupMenu(MouseEvent e) {
        if (e.isPopupTrigger()) {
            popupMenu.show(this, e.getX(), e.getY());
        }
    }
    protected FileExplorer getFileExplorer() {
        return fileExplorer;
    }

    public void setFileExplorer(FileExplorer fileExplorer) {
        this.fileExplorer = fileExplorer;
    }

}
