package com.qsz.GUI.Icon;

import com.qsz.Model.Directory;
import com.qsz.Exception.NodeNotExistException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by danielqiu on 6/3/14.
 */
public class DirectoryIcon extends NodeIcon {

    private Directory directory;
    private DirectoryIcon directoryIcon = this;




    public DirectoryIcon(Directory directory)
    {
        super(directory);

        this.image =  new ImageIcon(Directory.class.getResource("/folder_icon.png"));
        this.iconLabel.setIcon(this.image);

        this.directory = directory;

    }


    protected void addListener()
    {
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {

                if(e.isPopupTrigger())
                {
                    showPopupMenu(e);
                }

            }

            public void mouseReleased(MouseEvent e) {

                if(e.isPopupTrigger())
                {
                    showPopupMenu(e);
                }

            }

            public void mouseEntered(MouseEvent e) {
                directoryIcon.setBackground(Color.lightGray);

            }

            public void mouseExited(MouseEvent e) {
                directoryIcon.setBackground(null);
            }


        });

        this.deleteItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    directory.removeSelfFromParent();
                    fileExplorer.getDirectoryView().refreshView();
                } catch (NodeNotExistException e1) {
                    JOptionPane.showMessageDialog(fileExplorer,
                            e1.getMessage(),
                            "directory does not exist",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        this.renameItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String newDirectoryName = JOptionPane.showInputDialog("enter new directory name",directory.getName());
                if(newDirectoryName != null)
                {
                    directory.setName(newDirectoryName);
                }

                fileExplorer.getDirectoryView().refreshView();
            }
        });
    }





}
