package com.qsz.GUI;

import com.qsz.GUI.View.DetailView;
import com.qsz.GUI.View.DirectoryView;
import com.qsz.GUI.View.NavigationBar;
import com.qsz.GUI.View.ToolView;
import com.qsz.Model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Stack;

/**
 * Created by danielqiu on 6/2/14.
 */
public class FileExplorer extends JFrame implements ActionListener {

    private FileOutputStream fileOutputStream;
    private FileInputStream fileInputStream;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;

    private Container contentPane;
    private FileSystem fileSystem;
    private NavigationBar navigationBar;

    private ToolView toolView;
    private DetailView detailView;
    private DirectoryView directoryView;

    private Stack<Directory> backStack = new Stack<Directory>();
    private Stack<Directory> forwardStack = new Stack<Directory>();

    private Disk disk = new Disk();


    public FileExplorer()
    {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        contentPane = this.getContentPane();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        this.setSize(screenSize.width * 3 / 4, screenSize.height * 3 / 4);
        this.setResizable(false);
        this.setLocation((screenSize.width-this.getSize().width)/2, (screenSize.height-this.getSize().height)/2);
        this.setVisible(true);

        this.setTitle("File Explorer");

        this.contentPane.setLayout(null);

        this.initFileSystem();

        this.initNavigationBar(this.fileSystem);

        this.initToolView();

        this.initDetailView();

        this.initDirectoryView(this.fileSystem.getCurrentDirectory());




    }

    private void initNavigationBar(FileSystem fileSystem)
    {

        this.navigationBar = new NavigationBar(fileSystem);
        this.navigationBar.setBounds(0,0,this.getWidth(), this.getHeight()/10);
        contentPane.add(this.navigationBar);
        this.navigationBar.setFileExplorer(this);


    }

    private void initFileSystem()
    {

        try {
            this.readFileSystem();
        } catch (Exception e) {
            System.out.println("obj file not exists");
            this.fileSystem = new FileSystem();
            this.fileSystem.setFileExplorer(this);
            try {
                this.writeFileSystem();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }

    private void initToolView()
    {
        this.toolView = new ToolView(fileSystem);
        this.toolView.setFileExplorer(this);
        this.toolView.setBounds(0,this.navigationBar.getHeight(), this.getWidth()/5, this.getHeight()-this.navigationBar.getHeight());
        contentPane.add(this.toolView);


    }

    private void initDetailView()
    {
        this.detailView = new DetailView();
        this.detailView.setBounds(this.getWidth()*3/4, this.navigationBar.getHeight(), this.getWidth()/4, this.getHeight());
        contentPane.add(this.detailView);


    }

    private void initDirectoryView(Directory directory)
    {
        this.directoryView = new DirectoryView(this.fileSystem.getCurrentDirectory());
        this.directoryView.setFileExplorer(this);
        this.directoryView.setBounds(this.toolView.getWidth(), this.navigationBar.getHeight(), this.getWidth() - this.toolView.getWidth() - this.detailView.getWidth(), this.getHeight() - this.navigationBar.getHeight());
        contentPane.add(this.directoryView);
        this.directoryView.refreshView();
        this.toolView.setDirectoryView(this.directoryView);



    }

    private boolean readFileSystem() throws IOException, ClassNotFoundException {

        java.io.File file = new java.io.File("filesystem.obj");
        if(!file.exists())
        {
            throw new FileNotFoundException("object file not exist");
        }
        else
        {
            fileInputStream = new FileInputStream(file);
            objectInputStream = new ObjectInputStream(fileInputStream);
            this.fileSystem = (FileSystem) objectInputStream.readObject();
            this.fileSystem.setFileExplorer(this);
            System.out.println("read from file succeed");

        }
        return true;
    }

    public boolean writeFileSystem() throws IOException {
        fileOutputStream = new FileOutputStream("filesystem.obj");
        objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(this.fileSystem);
        System.out.println("write to file succeed");

        return true;
    }


    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public DirectoryView getDirectoryView() {
        return directoryView;
    }

    public void setDirectoryView(DirectoryView directoryView) {
        this.directoryView = directoryView;
    }

    public FileSystem getFileSystem() {
        return fileSystem;
    }

    public void setFileSystem(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    public DetailView getDetailView() {
        return detailView;
    }

    public void setDetailView(DetailView detailView) {
        this.detailView = detailView;
    }

    public ToolView getToolView() {
        return toolView;
    }

    public void setToolView(ToolView toolView) {
        this.toolView = toolView;
    }

    public NavigationBar getNavigationBar() {
        return navigationBar;
    }

    public void setNavigationBar(NavigationBar navigationBar) {
        this.navigationBar = navigationBar;
    }

    public Stack<Directory> getBackStack() {
        return backStack;
    }

    public void setBackStack(Stack<Directory> backStack) {
        this.backStack = backStack;
    }

    public Stack<Directory> getForwardStack() {
        return forwardStack;
    }

    public void setForwardStack(Stack<Directory> forwardStack) {
        this.forwardStack = forwardStack;
    }

    public Disk getDisk() {
        return disk;
    }

    public void setDisk(Disk disk) {
        this.disk = disk;
    }
}

