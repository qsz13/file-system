package com.qsz.GUI.View;


import com.qsz.GUI.FileExplorer;
import com.qsz.GUI.Icon.DirectoryIcon;
import com.qsz.GUI.Icon.FileIcon;
import com.qsz.Model.Directory;
import com.qsz.Model.File;
import com.qsz.Model.Node;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by danielqiu on 6/3/14.
 */
public class DirectoryView extends JPanel {

    private Directory dierectory;
    private FileExplorer fileExplorer;

    private List<Node> contentList = new ArrayList<Node>();




    public DirectoryView(Directory directory)
    {

        this.dierectory = directory;

        this.refreshView();

    }

    private void generateContentList()
    {
        contentList = new ArrayList<Node>();
        for(Directory d: this.dierectory.getSubDirectory())
        {
            this.contentList.add(d);
        }

        for(File f:this.dierectory.getSubFile())
        {
            this.contentList.add(f);
        }
    }

    public Directory getDierectory() {
        return dierectory;
    }

    public void setDierectory(Directory dierectory) {
        this.dierectory = dierectory;
        this.refreshView();

    }

    public void refreshView()
    {
        this.generateContentList();
        this.removeAll();
        for(final Node n:this.contentList)
        {
            final String name = n.getName();
            if (n instanceof Directory)
            {
                DirectoryIcon dIcon = new DirectoryIcon((Directory)n);
                dIcon.setFileExplorer(fileExplorer);

                final Directory dir = (Directory)n;
                dIcon.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (e.getClickCount() == 1) {
                            fileExplorer.getDetailView().setSelectedNode(dir);

                        }
                        else if(e.getClickCount() == 2)
                        {
                            try {
                                fileExplorer.getBackStack().push(fileExplorer.getFileSystem().getCurrentDirectory());
                                fileExplorer.getFileSystem().gotoDirectory(name);
                                fileExplorer.getForwardStack().clear();
                                System.out.println("into folder " + name);
                                refreshView();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }

                    }


                });
                this.add(dIcon);

            }
            else if(n instanceof File)
            {
                FileIcon fIcon = new FileIcon((File)n);
                fIcon.setFileExplorer(fileExplorer);

                final File file = (File)n;
                fIcon.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {

                        if (e.getClickCount() == 1) {

                            fileExplorer.getDetailView().setSelectedNode(file);


                        }
                        else if (e.getClickCount() == 2)
                        {
                            FileView fileView = new FileView(file);
                            file.getFcb().setDateLastAccessed(new Date());
                            fileView.setFileExplorer(fileExplorer);
                        }

                    }


                });
                this.add(fIcon);
            }


        }
        this.revalidate();
        this.repaint();
    }



    public void refreshSearchView(List<Node> resultList)
    {

        this.removeAll();
        for(final Node n:resultList)
        {
            final String name = n.getName();
            final String path = n.getPath().toString();
            if (n instanceof Directory)
            {
                DirectoryIcon dIcon = new DirectoryIcon((Directory)n);
                dIcon.setFileExplorer(fileExplorer);

                final Directory dir = (Directory)n;
                dIcon.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (e.getClickCount() == 1) {
                            fileExplorer.getDetailView().setSelectedNode(dir);

                        }
                        else if(e.getClickCount() == 2)
                        {
                            try {
                                fileExplorer.getBackStack().push(fileExplorer.getFileSystem().getCurrentDirectory());
                                fileExplorer.getFileSystem().gotoDirectory(path);
                                fileExplorer.getForwardStack().clear();
                                System.out.println("into folder " + name);
                                refreshView();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }

                    }


                });
                this.add(dIcon);

            }
            else if(n instanceof File)
            {
                FileIcon fIcon = new FileIcon((File)n);
                fIcon.setFileExplorer(fileExplorer);

                final File file = (File)n;
                fIcon.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {

                        if (e.getClickCount() == 1) {

                            fileExplorer.getDetailView().setSelectedNode(file);


                        }
                        else if (e.getClickCount() == 2)
                        {
                            FileView fileView = new FileView(file);
                            fileView.setFileExplorer(fileExplorer);
                        }

                    }


                });
                this.add(fIcon);
            }


        }
        this.revalidate();
        this.repaint();

    }




    public void removeAllFile()
    {
        this.dierectory.removeAll();
        this.refreshView();
    }


    public FileExplorer getFileExplorer() {
        return fileExplorer;
    }

    public void setFileExplorer(FileExplorer fileExplorer) {
        this.fileExplorer = fileExplorer;
    }
}
