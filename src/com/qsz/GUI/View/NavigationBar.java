package com.qsz.GUI.View;

import com.qsz.Exception.NodeNotExistException;
import com.qsz.GUI.FileExplorer;
import com.qsz.Model.FileSystem;
import com.qsz.Model.Directory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by danielqiu on 6/2/14.
 */
public class NavigationBar extends JPanel {

    private FileExplorer fileExplorer;
    private FileSystem fileSystem;

    private JButton backButton;
    private JButton forwardButton;
    private JButton gotoParentButton;

    private JPanel radioButtonPanel;
    private JPanel searchPanel;

    private JTextField pathTextField;
    private JButton gotoButton;

    private JTextField searchTextField;
    private JButton searchButton;
    private ButtonGroup buttonGroup;
    private JRadioButton rootRadioButton;
    private JRadioButton currentRadioButon;


    public NavigationBar(FileSystem fileSystem)
    {

        this.fileSystem = fileSystem;

        this.setBackground(Color.lightGray);
        this.setLayout(new FlowLayout());
        this.initNavigationButton();
        this.initPathModule();
        this.initSearchModule();



    }


    private void initNavigationButton()
    {
        this.backButton = new JButton("back");
        this.forwardButton = new JButton("forward");
        this.gotoParentButton = new JButton("up level");

        this.gotoParentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    fileSystem.gotoParentDirectory();
                } catch (NodeNotExistException e1) {
                    JOptionPane.showMessageDialog(fileExplorer,
                            e1.getMessage(),
                            "path error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        this.backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!fileExplorer.getBackStack().empty())
                {
                    Directory latestDir = fileExplorer.getBackStack().pop();

                    try {
                        fileExplorer.getForwardStack().push(fileSystem.getCurrentDirectory());
                        fileExplorer.getFileSystem().gotoDirectory(latestDir.getPath().toString());
                        fileExplorer.getDirectoryView().refreshView();

                    } catch (NodeNotExistException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        this.forwardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!fileExplorer.getForwardStack().empty())
                {
                    Directory latestDir = fileExplorer.getForwardStack().pop();

                    try {
                        fileExplorer.getBackStack().push(fileSystem.getCurrentDirectory());
                        fileExplorer.getFileSystem().gotoDirectory(latestDir.getPath().toString());
                        fileExplorer.getDirectoryView().refreshView();

                    } catch (NodeNotExistException e1) {
                        e1.printStackTrace();
                    }
                }

            }
        });

        this.add(this.backButton);
        this.add(this.forwardButton);
        this.add(this.gotoParentButton);



    }

    public void initPathModule()
    {

        this.pathTextField = new JTextField();
        this.pathTextField.setColumns(30);
        this.add(this.pathTextField);
        this.pathTextField.setText(this.fileSystem.getCurrentDirectory().getPath().toString());

        this.gotoButton = new JButton("go to");
        this.gotoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    fileExplorer.getBackStack().push(fileExplorer.getFileSystem().getCurrentDirectory());

                    fileExplorer.getFileSystem().gotoDirectory(pathTextField.getText());
                    fileExplorer.getForwardStack().clear();
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(fileExplorer,
                            e1.getMessage(),
                            "path error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        this.add(this.gotoButton);


    }

    public void initSearchModule()
    {

        this.searchTextField = new JTextField();
        this.searchTextField.setColumns(10);
        this.searchButton = new JButton("search");
        this.buttonGroup = new ButtonGroup();
        this.rootRadioButton = new JRadioButton("All Files");
        this.currentRadioButon = new JRadioButton("current");
        this.buttonGroup.add(this.rootRadioButton);
        this.buttonGroup.add(this.currentRadioButon);
        this.currentRadioButon.setSelected(true);

        this.radioButtonPanel = new JPanel(new FlowLayout());
        this.radioButtonPanel.setBackground(Color.lightGray);

        this.radioButtonPanel.add(this.rootRadioButton);
        this.radioButtonPanel.add(this.currentRadioButon);

        this.searchPanel = new JPanel();
        this.searchPanel.setLayout(new BoxLayout(this.searchPanel, BoxLayout.Y_AXIS));
        this.searchPanel.setBackground(Color.lightGray);
        this.searchPanel.add(this.searchTextField);
        this.searchPanel.add(this.radioButtonPanel);
        this.add(this.searchPanel);

        this.searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(rootRadioButton.isSelected())
                {
                    System.out.println("root");
                    fileSystem.searchInRootDirectory(searchTextField.getText());
                }
                else if(currentRadioButon.isSelected())
                {
                    fileSystem.searchInCurrentDirectory(searchTextField.getText());
                    System.out.println("current");
                }
            }
        });
        this.add(this.searchButton);

    }


    public void setCurrentDirectory(Directory directory)
    {
        this.pathTextField.setText(directory.getPath().toString());
    }

    public FileExplorer getFileExplorer() {
        return fileExplorer;
    }

    public void setFileExplorer(FileExplorer fileExplorer) {
        this.fileExplorer = fileExplorer;
    }

    public FileSystem getFileSystem() {
        return fileSystem;
    }

    public void setFileSystem(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }
}
