package com.qsz.GUI.View;

import com.qsz.Model.Node;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

/**
 * Created by danielqiu on 6/2/14.
 */
public class DetailView extends JPanel {




    private JTextArea detailTextArea = new JTextArea();
    private Border raisedetched;



    private Node selectedNode;

    public DetailView()
    {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.initBorder();

        this.add(this.detailTextArea);
        this.detailTextArea.setEnabled(false);

        this.detailTextArea.setText("\n\nName: \n\nType: \n\nSize: \n\nCreated: \n\nLast Accessed: \n\nLast Updated:\n\n");
        this.detailTextArea.setLineWrap(true);
        this.detailTextArea.setWrapStyleWord(true);



    }

    private void initBorder()
    {
        raisedetched = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
        this.setBorder(raisedetched);

    }

    public void updateView()
    {
        if(selectedNode == null)
        {
            return;
        }

    }


    public Node getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(Node selectedNode) {
        this.selectedNode = selectedNode;
        this.refreshView();
    }


    private void refreshView()
    {

        this.detailTextArea.setText("\n\nName: "+selectedNode.getName()+
                "\n\nType: "+selectedNode.getType()+
                "\n\nSize: "+selectedNode.getSize()+
                "\n\nCreated: "+selectedNode.getCreateTime()+
                "\n\nLast Accessed: "+selectedNode.getFcb().getDateLastAccessed()+
                "\n\nLast Updated: "+selectedNode.getFcb().getDateLastUpdated());


        this.revalidate();
        this.repaint();

    }


}
