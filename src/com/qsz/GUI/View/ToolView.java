package com.qsz.GUI.View;

import com.qsz.Exception.NameEmptyException;
import com.qsz.Exception.NodeExistsException;
import com.qsz.GUI.FileExplorer;
import com.qsz.Model.FileSystem;
import com.qsz.Model.Directory;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by danielqiu on 6/2/14.
 */
public class ToolView extends JPanel {

    private FileSystem fileSystem;

    private FileExplorer fileExplorer;

    private JButton newDirectoryButton;
    private JButton newFileButton;
    private JButton removeAllButton;

    private Border raisedetched;

    private DirectoryView directoryView;


    public ToolView(FileSystem fileSystem)
    {
        this.fileSystem = fileSystem;

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.initBorder();
        this.add(Box.createRigidArea(new Dimension(0, 50)));
        this.initNewDirectoryButton();
        this.add(Box.createRigidArea(new Dimension(0,20)));
        this.initNewFileButton();
        this.add(Box.createRigidArea(new Dimension(0,20)));
        this.initRemoveAllButton();
    }


    private void initBorder()
    {
        raisedetched = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
        this.setBorder(raisedetched);

    }

    private void initNewDirectoryButton()
    {
        this.newDirectoryButton = new JButton("new Directory");
        this.newDirectoryButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(this.newDirectoryButton);
        this.newDirectoryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showNewDirectoryDialog();
            }
        });
    }

    private void initNewFileButton()
    {
        this.newFileButton = new JButton("new File");
        this.newFileButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(this.newFileButton);
        this.newFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showNewFileDialog();
            }
        });

    }

    private void initRemoveAllButton()
    {
        this.removeAllButton = new JButton("remove all");
        this.removeAllButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(this.removeAllButton);
        this.removeAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showRemoveAllDialog();
            }
        });
    }


    private void showNewFileDialog()
    {
        String fileName= JOptionPane.showInputDialog("enter new file name");
        if(fileName != null)
        {
            this.createFile(fileName);
        }

    }

    private void showNewDirectoryDialog()
    {
        String direcrotyName = JOptionPane.showInputDialog("enter new directory name");
        if(direcrotyName != null)
        {
            this.createDiectory(direcrotyName);
        }

    }

    private void showRemoveAllDialog()
    {

        int optionResult = JOptionPane.showConfirmDialog(this.fileExplorer, "would you like to remove all the files?","warning",JOptionPane.YES_NO_OPTION);
        if(optionResult == JOptionPane.YES_OPTION)
        {
            this.directoryView.removeAllFile();

        }

    }



    public void createFile(String name) {
        Directory directory = this.fileSystem.getCurrentDirectory();
        try {
            directory.createFile(name);
            directoryView.refreshView();
            fileSystem.writeToFile();

        } catch (NameEmptyException e) {
            JOptionPane.showMessageDialog(this.fileExplorer,
                    e.getMessage(),
                    "Name Empty error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (NodeExistsException e) {
            JOptionPane.showMessageDialog(this.fileExplorer,
                    e.getMessage(),
                    "File exists error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void createDiectory(String name)
    {
        Directory directory = this.fileSystem.getCurrentDirectory();

        try {
            if(directory.createDirectory(name)) {
                directoryView.refreshView();
                fileSystem.writeToFile();
            }
        } catch (NameEmptyException e) {
            JOptionPane.showMessageDialog(this.fileExplorer,
                    e.getMessage(),
                    "Name Empty error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (NodeExistsException e) {
            JOptionPane.showMessageDialog(this.fileExplorer,
                    e.getMessage(),
                    "Directory exists error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }



    public DirectoryView getDirectoryView() {
        return directoryView;
    }

    public void setDirectoryView(DirectoryView directoryView) {
        this.directoryView = directoryView;
    }

    public FileExplorer getFileExplorer() {
        return fileExplorer;
    }

    public void setFileExplorer(FileExplorer fileExplorer) {
        this.fileExplorer = fileExplorer;
    }
}
