package com.qsz.GUI.View;

import com.qsz.Exception.NoSpaceException;
import com.qsz.GUI.FileExplorer;
import com.qsz.Model.File;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Date;

/**
 * Created by danielqiu on 6/3/14.
 */
public class FileView extends JFrame {

    private File file;

    private JFrame thisFrame = this;

    private String originalString;

    private JButton saveButton;
    private JButton clearButton;
    private JButton revertButton;
    private JButton cancelButton;
    private JPanel toolPanel = new JPanel(new FlowLayout());
    private JTextArea textArea = new JTextArea(5,5);

    private FileExplorer fileExplorer;

    public FileView(File file)
    {

        this.file = file;
        originalString = file.getFileContent();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        this.setSize(screenSize.width / 2, screenSize.height /2);
        this.setResizable(false);
        this.setLocation((screenSize.width - this.getSize().width) / 2, (screenSize.height - this.getSize().height) / 2);
        this.setVisible(true);
        this.setTitle(this.file.getName());
        this.setLayout(null);
        this.initToolBar();
        this.initContentView();
        this.initActionListener();

    }


    private void initToolBar()
    {
        this.toolPanel.setBounds(0,0,this.getWidth(), this.getHeight()/10);
//        this.toolPanel.setBackground(Color.red);
        this.add(this.toolPanel);
        this.saveButton = new JButton("save");
        this.clearButton = new JButton("clear");
        this.revertButton = new JButton("revert");
        this.cancelButton = new JButton("cancel");

        this.toolPanel.add(saveButton);
        this.toolPanel.add(clearButton);
        this.toolPanel.add(revertButton);
        this.toolPanel.add(cancelButton);



    }


    private void initActionListener()
    {
        this.saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                file.setFileContent(textArea.getText());
                try {
                    file.setBlockList(fileExplorer.getDisk().saveFile(textArea.getText())) ;
                } catch (NoSpaceException e1) {
                    JOptionPane.showMessageDialog(fileExplorer,
                            e1.getMessage(),
                            "disk space error",
                            JOptionPane.ERROR_MESSAGE);
                } catch (Exception e1) {

                }
                file.getFcb().setDateLastUpdated(new Date());
                try {
                    fileExplorer.writeFileSystem();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        });

        this.clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");
            }
        });

        this.revertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText(originalString);
            }
        });

        this.cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                thisFrame.dispose();
            }
        });






    }

    private void initContentView()
    {
        this.textArea.setBounds(this.getWidth()/20, this.toolPanel.getHeight() + this.getWidth()/20, this.getWidth() - this.getWidth()/10,this.getHeight()-(this.toolPanel.getHeight() + this.getWidth()/10));
        this.textArea.setText(this.file.getFileContent());
        this.add(this.textArea);
    }

    public FileExplorer getFileExplorer() {
        return fileExplorer;
    }

    public void setFileExplorer(FileExplorer fileExplorer) {
        this.fileExplorer = fileExplorer;
    }
}
