package com.qsz.Model;

import com.qsz.Exception.NodeNotExistException;
import com.qsz.GUI.FileExplorer;
import com.qsz.Model.Directory;
import com.qsz.Model.Node;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by danielqiu on 6/1/14.
 */
public class FileSystem implements Serializable {

    private Directory root = new Directory();

    private Directory currentDirectory = root;
    private transient FileExplorer fileExplorer;



    FileOutputStream fileOutputStream;
    private ObjectOutputStream objectOutputStream;



    public Directory getCurrentDirectory() {
        return currentDirectory;
    }

    public void setCurrentDirectory(Directory currentDirectory)
    {
        this.currentDirectory = currentDirectory;
    }

    private void writeObject(ObjectOutputStream outputStream) throws IOException {
        outputStream.writeObject(this.root);
        outputStream.writeObject(this.currentDirectory);

    }

    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        this.root = (Directory)inputStream.readObject();
        this.currentDirectory = (Directory)inputStream.readObject();
        this.currentDirectory.getFcb().setDateLastAccessed(new Date());

    }

    public void writeToFile() throws IOException {
        fileOutputStream = new FileOutputStream("filesystem.obj");
        objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(this);
        System.out.println("write to file succeed");
    }


    public Directory gotoDirectory(String pathString) throws NodeNotExistException {
        Path path = Paths.get(pathString);
        boolean isAbsolute = path.isAbsolute();
        int nameCount = path.getNameCount();
        Directory testDirectory;

        if(isAbsolute)
        {
            testDirectory = root;
        }
        else
        {
            testDirectory = currentDirectory;

        }

        for(int i = 0 ; i < nameCount; i++)
        {
            Path testSubDir = path.getName(i);
            testDirectory = testDirectory.gotoDirectory(testSubDir);
            if(testDirectory == null)
            {
                throw new NodeNotExistException("directory doesn't exist");
            }

        }

        this.currentDirectory = testDirectory;
        this.currentDirectory.getFcb().setDateLastAccessed(new Date());
        this.fileExplorer.getNavigationBar().setCurrentDirectory(this.currentDirectory);
        this.fileExplorer.getDirectoryView().setDierectory(this.currentDirectory);
        try {
            this.writeToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return testDirectory;

    }

    public boolean gotoParentDirectory() throws NodeNotExistException {
        Path path = this.currentDirectory.getPath();
        Path newPath = path.getParent();
        if(newPath != null)
        {
            fileExplorer.getBackStack().push(fileExplorer.getFileSystem().getCurrentDirectory());

            this.currentDirectory = this.gotoDirectory(newPath.toString());

            fileExplorer.getForwardStack().clear();
            this.currentDirectory.getFcb().setDateLastAccessed(new Date());
            return true;
        }
        else
        {
            throw new NodeNotExistException("no parent directory");
        }

    }

    public void searchInCurrentDirectory(String queryName){
        List<Node> searchResultList = new ArrayList<Node>();
        System.out.println("?????" + currentDirectory.getName());
        searchResultList = this.currentDirectory.search(queryName, searchResultList);
        this.fileExplorer.getDirectoryView().refreshSearchView(searchResultList);


    }

    public void searchInRootDirectory(String queryName){
        List<Node> searchResultList = new ArrayList<Node>();
        searchResultList = this.root.search(queryName, searchResultList);
        this.fileExplorer.getDirectoryView().refreshSearchView(searchResultList);




    }

    public FileExplorer getFileExplorer() {
        return fileExplorer;
    }

    public void setFileExplorer(FileExplorer fileExplorer) {
        this.fileExplorer = fileExplorer;
    }
}
