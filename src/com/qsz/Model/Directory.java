package com.qsz.Model;

import com.qsz.Exception.NameEmptyException;
import com.qsz.Exception.NodeExistsException;
import com.qsz.Exception.NodeNotExistException;

import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by danielqiu on 6/1/14.
 */
public class Directory extends Node implements Serializable {


    private List<Directory> subDirectory = new ArrayList<Directory>();
    private List<File> subFile = new ArrayList<File>();

    public Directory()
    {
        super("");
        this.setPath(Paths.get("/"));
    }

    public Directory(String name)
    {
        super(name);
    }

    @Override
    protected void setType(String name)
    {
        this.type = "directory";

    }

    public boolean createDirectory(String name) throws NodeExistsException, NameEmptyException {
        if(name == null)
        {
            return false;
        }
        if(name.length() <= 0)
        {
            throw new NameEmptyException("directory name cannot be empty");
        }
        else if(this.hasDuplicateDirectory(name))
        {
            throw new NodeExistsException("directory with the same name already exits");
        }
        else
        {
            Directory directory = new Directory(name);
            Path path = Paths.get(this.getPath().toString()+"/"+name);
            directory.setPath(path);
            directory.setParentDirectory(this);
            this.subDirectory.add(directory);
            this.getFcb().setDateLastUpdated(new Date());
            return true;
        }

    }

    public boolean removeDirectory(String name)
    {
        Iterator<Directory> i = this.subDirectory.iterator();
        while (i.hasNext()) {
            Directory directory = i.next();
            if (directory.getName().equals(name))
            {
                i.remove();
                return true;
            }

        }
        return false;

    }

    public boolean removeDirectory(Directory directory) throws NodeNotExistException {
        if(this.hasSubDirectory(directory))
        {
            this.subDirectory.remove(directory);

            return true;
        }
        else
        {
            throw new NodeNotExistException("file not exists");
        }

    }



    public boolean createFile(String name) throws NameEmptyException, NodeExistsException {
        if(name == null)
        {
            return false;
        }

        if(name.length() <= 0)
        {
            throw new NameEmptyException("File name are not supposed to be empty");
        }
        else if(this.hasDuplicateFile(name))
        {
            throw new NodeExistsException("file with the same name already exits");
        }
        else
        {
            File file = new File(name);
            Path path = Paths.get(this.getPath().toString()+"/"+name);
            file.setPath(path);
            file.setParentDirectory(this);
            this.subFile.add(file);
            this.getFcb().setDateLastUpdated(new Date());
            return true;
        }

    }

    public boolean removeFile(String name)
    {
        Iterator<File> i = this.subFile.iterator();
        while (i.hasNext()) {
            File file = i.next();
            if (file.getName().equals(name))
            {
                i.remove();
                return true;
            }

        }
        return false;

    }

    public boolean removeFile(File file) throws NodeNotExistException {
        if(this.hasSubFile(file))
        {

            this.subFile.remove(file);

            return true;
        }
        else
        {
            throw new NodeNotExistException("file not exists");
        }

    }


    public boolean hasDuplicateDirectory(String name)
    {
        for(Directory d: this.subDirectory)
        {
            if(d.getName().equals(name))
            {
                return true;
            }
        }

        return false;
    }

    public boolean hasDuplicateFile(String name)
    {
        for(File f: this.subFile)
        {
            if(f.getName().equals(name))
            {
                return true;
            }
        }
        return false;
    }

    public List<Node> search(String queryName, List<Node> resultList)
    {
        System.out.println(this.getName());
        if(queryName.length() <= 0)
        {
            return resultList;
        }

        for(File file: this.subFile)
        {
            if(file.getName().equals(queryName)||file.getName().contains(queryName))
            {
                resultList.add(file);
            }
        }
        for(Directory directory: this.subDirectory)
        {
            if(directory.getName().equals(queryName)||directory.getName().contains(queryName))
            {
                resultList.add(directory);
            }

            resultList = directory.search(queryName,resultList);
        }
        return resultList;
    }


    public Directory gotoDirectory(Path directory)
    {
        String directoryName = directory.toString();
        for (Directory d: this.subDirectory)
        {
            if (d.getName().equals(directoryName))
            {
                return d;

            }
        }
        return null;
    }

    public void removeAll()
    {


        this.subDirectory = new ArrayList<Directory>();
        this.subFile = new ArrayList<File>();
        
    }


    private boolean hasSubFile(File file)
    {
        for(File f:this.subFile)
        {
            if(f == file)
            {
                return true;
            }
        }
        return false;
    }

    private boolean hasSubDirectory(Directory directory) {

        for(Directory d:this.subDirectory) {
            if (d == directory) {
                return true;
            }
        }
        return false;
    }


    public void removeSelfFromParent() throws NodeNotExistException {

        this.parentDirectory.removeDirectory(this);
    }




    public List<Directory> getSubDirectory() {
        return subDirectory;
    }

    public void setSubDirectory(List<Directory> subDirectory) {
        this.subDirectory = subDirectory;
    }

    public List<File> getSubFile() {
        return subFile;
    }

    public void setSubFile(List<File> subFile) {
        this.subFile = subFile;
    }
}
