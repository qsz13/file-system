package com.qsz.Model;

import com.qsz.Exception.NodeNotExistException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by danielqiu on 6/1/14.
 */
public class File extends Node implements Serializable {

    private String fileContent = "";

    private List<Integer> blockList = new ArrayList<Integer>();

    public File(String name)
    {
        super(name);
    }

    @Override
    protected void setType(String name)
    {
        if(name.contains(".") && name.indexOf('.') != name.length()-1)
        {
            String fileExtension = this.getFileExtension(name);
            fileExtension = fileExtension.toLowerCase();
            Map<String, String> extDic = FileType.extensionDictionary;
            if(extDic.containsKey(fileExtension))
            {
                this.type = extDic.get(fileExtension);
            }
            else
            {
                this.type = "other";
            }
        }
        else
        {
            this.type = "other";
        }
    }

    private String getFileExtension(String name)
    {
        int indexOfDot = name.lastIndexOf('.');
        System.out.print(indexOfDot);
        String extension = name.substring(indexOfDot+1);
        return extension;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {

        this.setSize(fileContent.length());
        this.getFcb().setCurrentLength(fileContent.length());

        this.fileContent = fileContent;
    }


    public void removeSelfFromParent() throws NodeNotExistException {

        this.parentDirectory.removeFile(this);
    }

    public List<Integer> getBlockList() {
        return blockList;
    }

    public void setBlockList(List<Integer> blockList) {
        this.blockList = blockList;
    }

    public int getSize()
    {
        return this.fileContent.length();
    }
}
