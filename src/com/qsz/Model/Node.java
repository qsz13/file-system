package com.qsz.Model;


import com.qsz.Exception.NodeNotExistException;
import com.qsz.DataStructure.FCB;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

/**
 * Created by danielqiu on 6/1/14.
 */
public abstract class Node implements Serializable {

    protected Directory parentDirectory;
    private String name;
    private Path path;
    private FCB fcb;
    private Date createTime;
    protected String type;
    private int size;


    public Node(String name)
    {
        this.name = name;
        this.fcb = new FCB(name);
        this.createTime = new Date();
        this.setType(name);
        this.fcb.setDateLastAccessed(this.createTime);
        this.fcb.setDateLastUpdated(this.createTime);
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.fcb.setName(name);
        Path parentPath = this.path.getParent();
        String newPathString = parentPath.toString()+"/"+this.name;
        Path newPath = Paths.get(newPathString);
        this.path = newPath;
    }



    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }


    public FCB getFcb() {
        return fcb;
    }

    public void setFcb(FCB fcb) {
        this.fcb = fcb;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public String getType() {
        return type;
    }

    protected void setType(String name) {

    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private void writeObject(ObjectOutputStream outputStream) throws IOException {
        outputStream.writeObject(this.name);
        outputStream.writeObject(this.path.toString());
        outputStream.writeObject(this.fcb);
        outputStream.writeObject(this.createTime);
        outputStream.writeObject(this.type);
        outputStream.writeInt(this.size);


    }



    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {


        this.name = inputStream.readObject().toString();
        this.path = Paths.get(inputStream.readObject().toString());
        this.fcb = (FCB) inputStream.readObject();
        this.createTime = (Date) inputStream.readObject();
        this.type = inputStream.readObject().toString();
        this.size = inputStream.readInt();




    }


    public void removeSelfFromParent() throws NodeNotExistException {}

    public Directory getParentDirectory() {
        return parentDirectory;
    }

    public void setParentDirectory(Directory parentDirectory) {
        this.parentDirectory = parentDirectory;
    }


}
