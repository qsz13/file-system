package com.qsz.Model;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by danielqiu on 6/1/14.
 */
public class FileType {

    public static final Map<String, String> extensionDictionary = new HashMap<String, String>();
    static {
        extensionDictionary.put("7z","7-Zip");
        extensionDictionary.put("aac","audio");
        extensionDictionary.put("app","application");
        extensionDictionary.put("bmp","picture");
        extensionDictionary.put("bz2","bzip2");
        extensionDictionary.put("c","C/C++");
        extensionDictionary.put("cpp","C/C++");
        extensionDictionary.put("css","css");
        extensionDictionary.put("doc","document");
        extensionDictionary.put("exe","executable program");
        extensionDictionary.put("jpeg","image");
        extensionDictionary.put("png","image");
        extensionDictionary.put("mp3","media");
        extensionDictionary.put("mp4","media");
        extensionDictionary.put("m4a","media");
        extensionDictionary.put("m4v","media");
        extensionDictionary.put("sh","shell");
        extensionDictionary.put("java","java");









    }




}
