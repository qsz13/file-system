package com.qsz.Model;

import com.qsz.DataStructure.Block;
import com.qsz.Exception.NoSpaceException;
import com.qsz.Model.File;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danielqiu on 6/1/14.
 */
public class Disk {

    private List<Block> blockList = new ArrayList<Block>(1000);


    public Disk()
    {
        for (int i = 0; i < 1000; i++)
        {

            Block b = new Block(i);
            blockList.add(b);
        }
    }




    public List<Integer> saveFile(String fileContent, File file ) throws Exception {
        deleteFile(file);
        return saveFile(fileContent);
    }

    public List<Integer> saveFile(String fileContent) throws Exception {
        List<Integer> blockIndex = new ArrayList<Integer>();
        int indexNum = fileContent.length()/8+1;
        int beginIndex,endIndex;
        beginIndex = 0;
        endIndex = 7;
        if(fileContent.length() < 8)
        {
            endIndex = fileContent.length()-1;
        }

        for (int i = 0; i < indexNum; i++)
        {
            if(indexNum == i-1)
            {
                endIndex = fileContent.length()%8-1;
            }
            for (Block b : this.blockList)
            {

                if (b.isEmpty())
                {
                    b.setBlockContent(fileContent.substring(beginIndex,endIndex));
                    b.setEmpty(false);
                    blockIndex.add(b.getBlockID());
                    beginIndex+=8;
                    endIndex+=8;
                    continue;
                }

            }
            if(i <indexNum)
            {
                throw new NoSpaceException("Disk space not enough");
            }

        }

        return blockIndex;



    }

    public void deleteFile(File file)
    {
        List<Integer> indexList = file.getBlockList();

        for (Integer i : indexList)
        {
            blockList.get(i).setEmpty(true);

        }

    }






}
