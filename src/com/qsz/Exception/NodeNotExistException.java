package com.qsz.Exception;

/**
 * Created by danielqiu on 6/3/14.
 */
public class NodeNotExistException extends Exception {

    public NodeNotExistException(String message)
    {
        super(message);
    }

}
