package com.qsz.Exception;

/**
 * Created by danielqiu on 6/3/14.
 */
public class NameEmptyException extends Exception {


    public NameEmptyException(String message) {
        super(message);

    }
}
