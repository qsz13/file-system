package com.qsz.DataStructure;

import javax.swing.*;

/**
 * Created by danielqiu on 6/6/14.
 */
public class Block {

    private int blockID;
    private String blockContent = "";
    private boolean isEmpty;

    public Block(int id)
    {
        this.blockID = id;
        this.isEmpty = true;
    }


    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    public String getBlockContent() {
        return blockContent;
    }

    public void setBlockContent(String blockContent) {
        this.blockContent = blockContent;
    }

    public int getBlockID() {
        return blockID;
    }

    public void setBlockID(int blockID) {
        this.blockID = blockID;
    }
}
